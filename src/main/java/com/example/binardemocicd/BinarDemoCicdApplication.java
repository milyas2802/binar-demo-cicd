package com.example.binardemocicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BinarDemoCicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(BinarDemoCicdApplication.class, args);
	}

}
